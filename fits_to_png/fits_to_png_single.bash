#!/bin/bash
#------------------------------------------------------------------------------
#convert directroy with fits files to png format
#------------------------------------------------------------------------------
#user vars
OUTPUT_DIR=$1
OUTPUT_DIR_EQUALIZED=$2
INPUT_FILE_NAME=$3
#-----------------------------------------------
#TEXT_SIZE=35
#TEXT_POS_X=1700
#TEXT_POS_Y=2016
#TEXT_COLOR=yellow
#-----------------------------------------------
function process_file {  
  FILE_NAME=$(basename -- "$INPUT_FILE_NAME")
  FILE_NAME_NO_EXTENSION="${FILE_NAME%.*}"

  echo "  Processing file: " $FILE_NAME  
  OUTPUT_FILENAME=$OUTPUT_DIR/$FILE_NAME_NO_EXTENSION.png 
  OUTPUT_FILENAME_EQUALIZED=$OUTPUT_DIR_EQUALIZED/$FILE_NAME_NO_EXTENSION.png

  convert $INPUT_FILE_NAME $OUTPUT_FILENAME
  convert $INPUT_FILE_NAME -equalize $OUTPUT_FILENAME_EQUALIZED

  #convert $INPUT_FILE_NAME -equalize -pointsize $TEXT_SIZE -fill $TEXT_COLOR -draw "text 1700,2016 \"$FILE_NAME_NO_EXTENSION\"" $OUTPUT_DIR/$FILE_NAME_NO_EXTENSION.png
}
#-----------------------------------------------
#start of script
#-----------------------------------------------
process_file
#-----------------------------------------------
#end of script
#-----------------------------------------------
