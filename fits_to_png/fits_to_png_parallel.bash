#!/bin/bash
#------------------------------------------------------------------------------
#convert directroy with fits files to png format
#------------------------------------------------------------------------------
#user vars
INPUT_DIR=$1
OUTPUT_DIR=$INPUT_DIR/png
OUTPUT_DIR_EQUALIZED=$INPUT_DIR/png_equalized
FILE_EXTENSION=.fit
#-----------------------------------------------
#program vars
FILE_COUNT=0
START_TIME=$(date +'%s')
#------------------------------------------------------------------------------
#start of script
startTime=$(date +'%s')
echo "========================================================================="
echo $(date +"%Y-%m-%d:%Hh:%Mm:%Ss") 'Starting sctipt'
echo "========================================================================="

rm -fr $OUTPUT_DIR
mkdir $OUTPUT_DIR

rm -fr $OUTPUT_DIR_EQUALIZED
mkdir $OUTPUT_DIR_EQUALIZED


echo "Processing directory      :"$INPUT_DIR
echo "Output directory          :"$OUTPUT_DIR
echo "Output directory equalized:"$OUTPUT_DIR_EQUALIZED

find $INPUT_DIR -maxdepth 1 -type f -name *$FILE_EXTENSION | parallel --bar  bash ./fits_to_png_single.bash $OUTPUT_DIR $OUTPUT_DIR_EQUALIZED

echo "========================================================================="
echo $(date +"%Y-%m-%d:%Hh:%Mm:%Ss") 'End of script'
echo "========================================================================="
echo "Elapsed time: $(($(date +'%s') - $startTime))s"
#-----------------------------------------------
#end of script
#-----------------------------------------------
