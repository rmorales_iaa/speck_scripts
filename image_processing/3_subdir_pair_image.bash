#!/bin/bash
#--------------------------------------------
#user's input
ROOT_DIR_0=/media/data/astrometry/matilde/batch_2/edge
ROOT_DIR_1=/media/data/astrometry/matilde/batch_2/edge/flat_images/
ROOT_DIR_2=/media/data/astrometry/matilde/batch_2/edge/science_images/
#--------------------------------------------
function work() {
   
  ROOT_DIR=$1

  DIR_LIST="$(find $ROOT_DIR -maxdepth 1 -type d -name "filter_"* | sort)"
  for DIR in $DIR_LIST; do
     ./pair_image_parallel.bash $DIR
  done
}
#--------------------------------------------
startTime=$(date +'%s')

work $ROOT_DIR_0
work $ROOT_DIR_1
work $ROOT_DIR_2

echo "---------->Elapsed time  : $(($(date +'%s') - $startTime))s"
#--------------------------------------------
#--------------------------------------------
#end of script
#--------------------------------------------

