#!/bin/bash
#--------------------------------------------
#start of script
#dependeces
# debian:
#   sudo apt install imagemagick
#------------------------------------------------------------------------------   
#user's variables and functions
OUTPUT_DIR=$1
MACRO_NAME=$2
INPUT_FILE=$3
#------------------------------------------------------------------------------
IMAGE_J_DIR="imagej/"
DIVIDER="#"
#------------------------------------------------------------------------------
function work() {
 
  FILE_NAME=$(basename -- "$INPUT_FILE")
  FILE_NAME_NO_EXTENSION="${FILE_NAME%.*}"
  OUTPUT_FILE_NAME=$OUTPUT_DIR/$FILE_NAME_NO_EXTENSION.png

  echo "---->Processing file: '$FILE_NAME'" 
  
  #calculate  
  cd $IMAGE_J_DIR
  java -jar ij.jar -batch $MACRO_NAME $INPUT_FILE$DIVIDER$OUTPUT_FILE_NAME
}
#------------------------------------------------------------------------------
if [ -n "$INPUT_FILE" ] 
 then work
 else echo "No input file to process" 
fi
#------------------------------------------------------------------------------
#end of script
#------------------------------------------------------------------------------
