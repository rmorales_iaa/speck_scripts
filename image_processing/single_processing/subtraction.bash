#!/bin/bash
#--------------------------------------------
#start of script
#dependeces
# debian:
#   my_imstat is a adapted version of cftsio utility of imstat: https://heasarc.gsfc.nasa.gov/docs/software/fitsio/cexamples.html
#------------------------------------------------------------------------------   
#user's variables and functions
INPUT=$1
#------------------------------------------------------------------------------
IMAGE_J_DIR="imagej/"
MACRO_NAME=subtraction.txt
MACRO_ARGUMENT_DIVIDER=#
#------------------------------------------------------------------------------
OUTPUT_FILE_DIVIDER="-"
#------------------------------------------------------------------------------
#https://stackoverflow.com/questions/369758/how-to-trim-whitespace-from-a-bash-variable
trim() {
  local s2 s="$*"
  until s2="${s#[[:space:]]}"; [ "$s2" = "$s" ]; do s="$s2"; done
  until s2="${s%[[:space:]]}"; [ "$s2" = "$s" ]; do s="$s2"; done
  echo "$s"
}
#------------------------------------------------------------------------------
function work() {

  #get the individual parameteters from the call
  IFS='#' read -ra split_array <<< "$INPUT"
  
  OUTPUT_DIR="${split_array[0]}"
  INPUT_FILE_A="${split_array[1]}"
  INPUT_FILE_B="${split_array[2]}"
 
  FILE_NAME_A=$(basename -- "$INPUT_FILE_A")
  FILE_NAME_B=$(basename -- "$INPUT_FILE_B")
  FILE_NAME_A_NO_EXTENSION="${FILE_NAME_A%.*}"
  FILE_NAME_B_NO_EXTENSION="${FILE_NAME_B%.*}"
  OUTPUT_FILE_NAME=$OUTPUT_DIR/$FILE_NAME_B_NO_EXTENSION$OUTPUT_FILE_DIVIDER$FILE_NAME_A_NO_EXTENSION".fits"
  
  #process the image files. All output will be redirect output csv file, so avoid the next message
  #echo "---->Processing files: '$FILE_NAME_B_NO_EXTENSION' subtract '$FILE_NAME_A_NO_EXTENSION'"
  
  #calculate the operation between images  
  OLD_DIR=$(pwd)
  cd $IMAGE_J_DIR
  java -jar ij.jar -batch $MACRO_NAME $INPUT_FILE_A$MACRO_ARGUMENT_DIVIDER$INPUT_FILE_B$MACRO_ARGUMENT_DIVIDER$OUTPUT_FILE_NAME
  cd $OLD_DIR
  
  #get the stat of the image result. it will be written on the output csv file in parallel script
  STAT_OUPUT=$(./my_imstat $OUTPUT_FILE_NAME)
  
  echo "$FILE_NAME_B_NO_EXTENSION $FILE_NAME_A_NO_EXTENSION $STAT_OUPUT"  
}
#------------------------------------------------------------------------------
if [ -n "$INPUT" ] 
 then work
 else echo "No input to process" 
fi
#------------------------------------------------------------------------------
#end of script
#------------------------------------------------------------------------------
