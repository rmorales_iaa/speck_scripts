#!/bin/bash
#--------------------------------------------
#start of script
#dependeces
# debian:
#   sudo apt install gnuastro
#------------------------------------------------------------------------------   
#user's variables and functions
OUTPUT_DIR_FLAT=$1     #directory to store flat images
OUTPUT_DIR_SCIENCE=$2  #directory to store science images
INPUT_FILE=$3          #image to slassify
#------------------------------------------------------------------------------
function get_filter_name() {  
  FITS_RECORD_FILTER_NAME="$(astfits $INPUT_FILE --hdu=0  | grep 'INSFLNAM')"
  FILTER_NAME="${FITS_RECORD_FILTER_NAME:19:1}"
  echo "filter_$FILTER_NAME"
}
#------------------------------------------------------------------------------
function link_file_to_directory() {
  ODIR=$1
  if [[ ! -e $ODIR ]]; then
    mkdir -p $ODIR
  fi  
  ln -s $INPUT_FILE $ODIR
}
#------------------------------------------------------------------------------
function work() {
  IMAGE_NAME=$(basename -- "$INPUT_FILE")
  FITS_RECORD_VALUE="$(astfits $INPUT_FILE --hdu=0  | grep 'HIERARCH CAHA DET EXP TYPE' | grep 'flat')"  
  FILTER_NAME=$(get_filter_name)

  if [ -z "$FITS_RECORD_VALUE" ]
  then
    echo "-------->file: '$IMAGE_NAME' is a SCIENCE image file with filter '$FILTER_NAME'"
    link_file_to_directory $OUTPUT_DIR$OUTPUT_DIR_SCIENCE/$FILTER_NAME    
  else
    echo "-------->file: '$IMAGE_NAME' is a FLAT image file with filter '$FILTER_NAME'"
    link_file_to_directory $OUTPUT_DIR$OUTPUT_DIR_FLAT/$FILTER_NAME  
  fi
}
#------------------------------------------------------------------------------
work
#------------------------------------------------------------------------------
#end of script
#------------------------------------------------------------------------------
