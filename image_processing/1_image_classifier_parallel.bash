#!/bin/bash
#--------------------------------------------
#start of script
#dependeces
# debian
#   sudo apt install parallel   
#--------------------------------------------
#user's variables and functions
INPUT_DIR=/media/data/astrometry/matilde/batch_2/edge  #directory with the input files not classified
OUTPUT_DIR_FLAT=$INPUT_DIR/flat_images
OUTPUT_DIR_SCIENCE=$INPUT_DIR/science_images
#--------------------------------------------
SINGLE_BASH_PROGRAM=single_processing/classifier_image.bash
FILE_EXTENSION=".fits"
#--------------------------------------------
echo "Preparing directory: '$OUTPUT_DIR_FLAT'"
rm -fr $OUTPUT_DIR_FLAT
mkdir -p $OUTPUT_DIR_FLAT

echo "Preparing directory: '$OUTPUT_DIR_SCIENCE'"
rm -fr $OUTPUT_DIR_SCIENCE
mkdir -p $OUTPUT_DIR_SCIENCE
#--------------------------------------------
echo "User parameters:"
echo "  Input dir               :" $INPUT_DIR
echo "  Flat image directory    :" $OUTPUT_DIR_FLAT
echo "  Science image directory :" $OUTPUT_DIR_SCIENCE
echo "  File extension          :" $FILE_EXTENSION
echo "  User batch file         :" $SINGLE_BASH_PROGRAM
#--------------------------------------------
echo "Parallelizating the jobs. It will take some seconds..."
#--------------------------------------------
./dropcaches_system_ctl 
startTime=$(date +'%s')

find $INPUT_DIR -maxdepth 1 -type f -name *$FILE_EXTENSION | parallel --bar  bash ./$SINGLE_BASH_PROGRAM $OUTPUT_DIR_FLAT $OUTPUT_DIR_SCIENCE 

INPUT_DIR_COUNT="$(find $INPUT_DIR -maxdepth 1 -type f -name *"$FILE_EXTENSION" -printf x | wc -c)"
OUTPUT_DIR_FLAT_COUNT="$(find -L $OUTPUT_DIR_FLAT -maxdepth 3 -type f -name *"$FILE_EXTENSION" -printf x | wc -c)"
OUTPUT_DIR_SCIENCE_COUNT="$(find -L $OUTPUT_DIR_SCIENCE -maxdepth 3 -type f -name *"$FILE_EXTENSION" -printf x | wc -c)"

echo "---------->Input directory file count         :" $INPUT_DIR_COUNT
echo "---------->Flat image directory file count    :" $OUTPUT_DIR_FLAT_COUNT 
echo "---------->Science image directory file count :" $OUTPUT_DIR_SCIENCE_COUNT 

echo "---------->Elapsed time                       : $(($(date +'%s') - $startTime))s"
./dropcaches_system_ctl 
#--------------------------------------------
#end of script
#--------------------------------------------
