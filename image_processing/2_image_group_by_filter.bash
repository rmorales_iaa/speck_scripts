#!/bin/bash
#--------------------------------------------
#start of script
#dependeces
# debian
#   sudo apt install parallel   
#--------------------------------------------
#user's variables and functions
INPUT_DIR=/media/data/astrometry/matilde/batch_2/edge  #directory with the input files not classified
OUTPUT_DIR=$INPUT_DIR
INPUT_DIR_FLAT=$INPUT_DIR/flat_images
INPUT_DIR_SCIENCE=$INPUT_DIR/science_images
#--------------------------------------------
FILE_EXTENSION=".fits"
#--------------------------------------------
echo "User parameters:"
echo "  Input dir               :" $INPUT_DIR
echo "  Flat image directory    :" $INPUT_DIR_FLAT
echo "  Science image directory :" $INPUT_DIR_SCIENCE
echo "  File extension          :" $FILE_EXTENSION
echo "  Output dir              :" $OUTPUT_DIR
#--------------------------------------------
startTime=$(date +'%s')

SUB_DIR_FLAT_SEQ=($INPUT_DIR_FLAT/*)
SUB_DIR_SCIENCE_SEQ=($INPUT_DIR_SCIENCE/*)

SUB_DIR_FLAT_SEQ_LEN=${#SUB_DIR_FLAT_SEQ[@]}
SUB_DIR_SCIENCE_SEQ_LEN=${#SUB_DIR_SCIENCE_SEQ[@]}

#------------------------------------------------------------------------------
function link_file_to_directory() {
  SUB_DIR_FLAT=$1
  SUB_DIR_SCIENCE=$2
  ODIR=$3

  if [ "$INPUT_DIR" = "$ODIR" ]
    then
      echo "Error processing an empty filter"
    else  
      echo  "Creating directory: '$ODIR'"
      rm -fr $ODIR
      mkdir $ODIR

     
     for FILE in $SUB_DIR_FLAT/*$FILE_EXTENSION
       do         
	     ln -s $FILE $ODIR       
       done      


     for FILE in $SUB_DIR_SCIENCE/*$FILE_EXTENSION
       do
	     ln -s $FILE $ODIR 
       done      
  fi  
}
#------------------------------------------------------------------------------
#check sub-dir count
if [ "$SUB_DIR_FLAT_SEQ_LEN" -eq "$SUB_DIR_SCIENCE_SEQ_LEN" ]
  then

    for (( i=0; i<$SUB_DIR_FLAT_SEQ_LEN; i++ ))
      do 
        SUB_DIR_FLAT="${SUB_DIR_FLAT_SEQ[$i]}" 
      	SUB_DIR_SCIENCE="${SUB_DIR_SCIENCE_SEQ[$i]}" 
		FILTER_NAME=$(basename $SUB_DIR_FLAT)
        ODIR=$OUTPUT_DIR/$FILTER_NAME

        link_file_to_directory $SUB_DIR_FLAT $SUB_DIR_SCIENCE $ODIR

      done
  else  
    echo "The number of flat and science sub-directories are different"
fi

echo "---------->Elapsed time                       : $(($(date +'%s') - $startTime))s"
#--------------------------------------------
#end of script
#--------------------------------------------
