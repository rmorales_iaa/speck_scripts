#!/bin/bash
#--------------------------------------------
#start of script
#dependeces
# debian
#   sudo apt install parallel   
#--------------------------------------------
#user's variables and functions
INPUT_DIR=$1 #/media/data/astrometry/matilde/batch_2/edge/  #directory with flat images
OUTPUT_DIR=$INPUT_DIR/subtraction #output directory
SINGLE_BASH_PROGRAM=single_processing/subtraction.bash
OUTPUT_CSV_FILE=$OUTPUT_DIR/"subtraction.csv"
#--------------------------------------------
FILE_EXTENSION=".fits"
DIVIDER=#
#--------------------------------------------
echo "Preparing directory: '$OUTPUT_DIR'"
rm -fr $OUTPUT_DIR
mkdir -p $OUTPUT_DIR
#--------------------------------------------
echo "User parameters:"
echo "  Input dir       :" $INPUT_DIR
echo "  Output dir      :" $OUTPUT_DIR
echo "  Output csv file :" $OUTPUT_CSV_FILE
echo "  File extension  :" $FILE_EXTENSION
echo "  User batch file :" $SINGLE_BASH_PROGRAM
#--------------------------------------------
startTime=$(date +'%s')

INPUT_DIR_COUNT="$(find -L $INPUT_DIR -maxdepth 1 -type f -name *"$FILE_EXTENSION" -printf x | wc -c)"
FILE_NAME_LIST="$(find -L $INPUT_DIR -maxdepth 1 -type f -name *"$FILE_EXTENSION" | sort)"

#gather all the filenames to process
file_name_array=($INPUT_DIR/*$FILE_EXTENSION)
INPUT_DIR_COUNT=${#file_name_array[@]}
echo "Procesing $INPUT_DIR_COUNT image files"

#--------------------------------------------
./dropcaches_system_ctl 

echo "Parallelizating the jobs. It will take some seconds..."
#--------------------------------------------
#join all the parameters to call $SINGLE_BASH_PROGRAM into a string. Store all strings into an array
CALL_ARRAY=()
for (( i=0,j=1; i < (( $INPUT_DIR_COUNT-1 )); i++,j++ )); do 
     CALL_ARRAY[$i]=$OUTPUT_DIR$DIVIDER${file_name_array[$i]}$DIVIDER${file_name_array[$j]}     
done

#write header on output file
echo "left_image right_image min max avg std pix_non_zero" > $OUTPUT_CSV_FILE

#parallelize all the calls and cordinate the outputs to write into OUTPUT_CSV_FILE
parallel --bar bash $SINGLE_BASH_PROGRAM ::: "${CALL_ARRAY[@]}" >> $OUTPUT_CSV_FILE

#print summary
OUTPUT_DIR_COUNT="$(find  $OUTPUT_DIR -maxdepth 1 -type f -name *"$FILE_EXTENSION" -printf x | wc -c)"
echo "---------->Input directory file count ": $INPUT_DIR_COUNT
echo "---------->Output directory file count": $OUTPUT_DIR_COUNT
echo "---------->Output csv file            ": $OUTPUT_CSV_FILE
echo "---------->Elapsed time               ": $(($(date +'%s') - $startTime))s

./dropcaches_system_ctl 
#--------------------------------------------
#end of script
#--------------------------------------------
