#!/bin/bash
#--------------------------------------------
#start of script
#dependeces
# debian
#   sudo apt install parallel   
#--------------------------------------------
#user's variables and functions
INPUT_DIR=/media/data/astrometry/matilde/batch_2/ #directory with the input files 
OUTPUT_DIR=$INPUT_DIR/edge/ #output directory
#--------------------------------------------
SINGLE_BASH_PROGRAM=single_processing/image_j_macro.bash
EXTRA_ARGUMENT=speck_detection.txt  #in case of 'image_j_edge_detector.bash' this is the macro name. In other case set it as blank
INPUT_FILE_EXTENSION=".fits"
#--------------------------------------------
echo "Preparing directory: '$OUTPUT_DIR'"
rm -fr $OUTPUT_DIR
mkdir -p $OUTPUT_DIR
#--------------------------------------------
echo "User parameters:"
echo "  Input dir               : '$INPUT_DIR'"
echo "  Output image directory  : '$OUTPUT_DIR'"
echo "  File extension          : '$FILE_EXTENSION'"
echo "  User's batch file       : '$SINGLE_BASH_PROGRAM'"
#--------------------------------------------
echo "Parallelizating the jobs. It will take some seconds..."
#--------------------------------------------
./dropcaches_system_ctl 
startTime=$(date +'%s')

find $INPUT_DIR -maxdepth 1 -type f -name *$INPUT_FILE_EXTENSION | parallel --bar bash $SINGLE_BASH_PROGRAM $OUTPUT_DIR $EXTRA_ARGUMENT

INPUT_DIR_COUNT="$(find $INPUT_DIR -maxdepth 1 -type f -name *"$INPUT_FILE_EXTENSION" -printf x | wc -c)"
OUTPUT_DIR_COUNT="$(find -L $OUTPUT_DIR -maxdepth 1 -type f -name *"$INPUT_FILE_EXTENSION" -printf x | wc -c)"

echo "---------->Input directory file count         :" $INPUT_DIR_COUNT
echo "---------->Output image directory file count  :" $OUTPUT_DIR_COUNT
echo "---------->Elapsed time                       : $(($(date +'%s') - $startTime))s"
./dropcaches_system_ctl 
#--------------------------------------------
#end of script
#--------------------------------------------
